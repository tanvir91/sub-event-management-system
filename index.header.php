<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="TemplateMo">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <title>SUB EMS</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Additional CSS Files -->
   
  <!--<link rel="stylesheet" type="text/css "href="css/style.css"> -->

<link href="https://fonts.googleapis.com/css2?family=Ibarra+Real+Nova&family=Josefin+Sans:wght@100&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
      <link href="css/styles.css" rel="stylesheet" /> 
        
		 
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"  />
	 
	 
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css"  />
 <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css"  /> 
 


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css"  />
	  
	

<link href="https://fonts.googleapis.com/css2?family=Ibarra+Real+Nova&family=Josefin+Sans:wght@100&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-edu-meeting.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <link rel="stylesheet" href="assets/css/lightbox.css">
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<style>
.header-area .main-nav {
  min-height: 100px;
  
  background: transparent;
 font-color:#100b3d;
}

.header-area .main-nav .logo {
  line-height: 100px;
  color: #100b3d;
  font-size: 28px;
  font-weight: 700;
  text-transform: uppercase;
  letter-spacing: 2px;
  float: left;
  -webkit-transition: all 0.3s ease 0s;
  -moz-transition: all 0.3s ease 0s;
  -o-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}
.header-area .main-nav .nav li a {
    display: block;
    font-weight: 500;
    font-size: 14px;
    text-transform: uppercase;
    color: #fff;
    -webkit-transition: all 0.3s ease 0s;
    -moz-transition: all 0.3s ease 0s;
    -o-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
    height: 40px;
    line-height: 40px;
    border: transparent;
    letter-spacing: 1px;
}



.header-area .main-nav .nav li:hover a,
.header-area .main-nav .nav li a.active {
  color: #f5a425!important;
}

.background-header .main-nav .nav li:hover a,
.background-header .main-nav .nav li a.active {
  color: #f5a425!important;
  opacity: 1;
}

.header-area .main-nav .nav li.has-sub {
  position: relative;
  padding-right: 15px;
}

.header-area .main-nav .nav li.has-sub:after {
  font-family: FontAwesome;
  content: "\f107";
  font-size: 12px;
  color: #fff;
  position: absolute;
  right: 5px;
  top: 12px;
}

.background-header .main-nav .nav li.has-sub:after {
  color: #1e1e1e;
}

.header-area .main-nav .nav li.has-sub ul.sub-menu {
  position: absolute;
  width: 200px;
  box-shadow: 0 2px 28px 0 rgba(0, 0, 0, 0.06);
  overflow: hidden;
  top: 40px;
  opacity: 0;
  transition: all .3s;
  transform: translateY(+2em);
  visibility: hidden;
  z-index: -1;
  
  
}


  	table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: 
}

th:nth-child(even),td:nth-child(even) {
  background-color: 
}

th, td {
  padding-top: 10px;
  padding-bottom: 5px;
  padding-left: 20px;
  padding-right: 40px;
}
a.nav-link:hover, a.nav-link:active {
  background-color: green;
  color: black;
}
 

</style>
<!--

TemplateMo 569 Edu Meeting

https://templatemo.com/tm-569-edu-meeting

-->
  </head>

<body>

  <!-- Sub Header -->
 <!--<div class="sub-header">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-sm-8">
          <div class="left-content">
            <p>Sub Ems <em>....</em> .....</p>
          </div>
        </div>
         <div class="col-lg-4 col-sm-4">
          <div class="right-icons">
            <ul>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
             
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <nav class="main-nav">
                      <!-- ***** Logo Start ***** -->
                      <a href="index.html" class="logo">
					   <img src="assets/images/download.png" alt="" width="90" height="65" class="d-inline-block align-text-top">
                         
                      </a>
                      <!-- ***** Logo End ***** -->
                      <!-- ***** Menu Start ***** -->
                      <ul class="nav">
                          <li><a href="index.php">Home</a></li>
                          <li><a href="event_archive.php">Archive</a></li>
                          <li class="scroll-to-section"><a href="#apply">Department</a></li>
                          <li class="has-sub">
                              <a href="javascript:void(0)">Register</a>
                              <ul class="sub-menu">
                                  <li><a href="user_registration.php">Users Registration</a></li>
                                  
                              </ul>
                          </li>
						  <li class="has-sub">
                              <a href="javascript:void(0)">Sign IN</a>
                              <ul class="sub-menu">
                                  <li><a href="users_login.php">USERS </a></li>
                                  <li><a href="su_admin_login.php">Super Admin</a></li>
								  <li><a href="dept_admin_login.php">Dept Admin</a></li>
                              </ul>
                          </li>
                          
                          <li class="scroll-to-section"><a href="#contact">About Us</a></li> 
                      </ul>        
                      <a class='menu-trigger'>
                          <span>Menu</span>
                      </a>
                      <!-- ***** Menu End ***** -->
                  </nav>
              </div>
          </div>
      </div>
  </header>
  
    <section class="heading-page header-text" id="top">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h6></h6>
          <h2>SUB EMS</h2>
        </div>
      </div>
    </div>
  </section>
  