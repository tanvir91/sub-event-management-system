<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
       <!-- <link href="css/styles.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"  />
	 
	  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css"  />
	  
	 
  
 
 
  <!--<link rel="stylesheet" type="text/css "href="css/style.css"> -->
<link rel="stylesheet" type="text/css "href="css/calorsol.css"> 
<link href="https://fonts.googleapis.com/css2?family=Ibarra+Real+Nova&family=Josefin+Sans:wght@100&display=swap" rel="stylesheet">

  <style>	
	
	
	
		table, th,td {
  border: 1px solid black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: rgba(150, 212, 212, 0.4);
}



 th,td {
  padding-top: 2px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 15px;
  font-weight: bold;
  font-size: 11px;
  
  
}
th {
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 15px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 12px;
  
  
}
.eventperparti th, .eventperparti td {
  padding-top: 8px;
  padding-bottom: 5px;
  padding-left: 1px;
  padding-right: 8px;
  font-weight: bold;
  font-size: 16px;
  
  
}
.eventperparti th {
  padding-top: 8px;
  padding-bottom: 5px;
  padding-left: 1px;
  padding-right: 8px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 16px;
  
  
}

.statusperuser th, .statusperuser td {
  padding-top: 8px;
  padding-bottom: 5px;
  padding-left: 1px;
  padding-right: 8px;
  font-weight: bold;
  font-size: 16px;
  
  
}
.statusperuser th {
  padding-top: 8px;
  padding-bottom: 5px;
  padding-left: 1px;
  padding-right: 8px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 16px;
  
  
}

.dis th, .dis td {
  padding-top: 8px;
  padding-bottom: 5px;
  padding-left: 1px;
  padding-right: 8px;
  font-weight: bold;
  font-size: 14px;
  
  
}
.dis th {
  padding-top: 8px;
  padding-bottom: 5px;
  padding-left: 1px;
  padding-right: 8px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 14px;
  
  
}


.ss th, .ss td {
  padding-top: 8px;
  padding-bottom: 6px;
  padding-left: 1px;
  padding-right: 10px;
  font-weight: bold;
  font-size: 14px;
  
  
}
.ss th {
  padding-top: 8px;
  padding-bottom: 6px;
  padding-left: 1px;
  padding-right: 10px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 14px;
  
  
}



a.view_part:link, a.view_part:visited {
  background-color: #92196c;
  color: black;
  padding: 6px 8px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.view_part:hover, a.view_part:active {
  background-color: blue;
}


a.upadmin:link, a.upadmin:visited {
  background-color: #199249;
  color: black;
  padding: 8px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.upadmin:hover, a.upadmin:active {
  background-color: blue;
}


a.up11pass:link, a.up11pass:visited {
  background-color: #198a92;
  color: black;
  padding: 8px 9px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.up11pass:hover, a.up11pass:active {
  background-color: blue;
}

a.dlt:link, a.dl_dp_admin:visited {
  background-color: #FF0000;
  color: black;
  padding: 6px 12px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.dlt:hover, a.dlt:active {
  background-color: blue;
}
</style>
 
</head>
<body>




<!-- nav Start-->
 
<!-- nav close-->


<!--  carosal-->


<nav class="navbar navbar-expand-lg navbar-dark bg-success">

 <a class="navbar-brand" href="#">
      <img src="image\download.png" alt="" width="200" height="70" class="d-inline-block align-text-top">
     
    </a>
    <a class="navbar-brand" href="#">SUB EMS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 





  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">HOME<span class="sr-only">(current)</span></a>
      </li>


       

          <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          DEPARTMENT
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
         
          <a class="dropdown-item" href="#"> <?php echo $dept_code;?></a>
          

</div>
          </li>
        <li class="nav-item active">
        <a class="nav-link" href="#">VIEW EVENT</a>
      </li>   
     

      


      <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          PROFILE
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		<a class="dropdown-item" href="#"> <?php echo $active_user['user_id'];?></a>
          <a class="dropdown-item" href="#">Change Profile</a>
          
           
          

</div>
         </li>
		 
		    <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          LOGOUT
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		
          <a class="dropdown-item" href="4volunteer_user_logout.php">Logout</a>
           
          

</div>
        


    </ul>
    
  </div>
</nav>


<div id="layoutSidenav_content">
                <main>

<div class="container-fluid px-4">