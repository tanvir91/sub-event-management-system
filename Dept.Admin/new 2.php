 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note:</h5>
              This page has been enhanced for downloading PDF and printing. Click the  button at the bottom of the invoice to test.
            </div>


            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class=""><img src="../image/download.png" alt="Visa" width="150" height="50"></i>
					
                    <small class="float-right"><?php date_default_timezone_set('Asia/Dhaka');
					echo $date = date('m/d/Y h:i:s a', time());
					 ?></small>
					<br><br>Event Invoice:<?php $res1=mysqli_query($con,"select *from  event where event_id='$event_id'");
          while($row=mysqli_fetch_assoc($res1)){ echo $row['event_id'];
		  $event_date=$row['start_date'];} ?>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  
                   <address>
                    <strong>State University of Bangladesh.<br>Dept. of <?php echo $dept_code;  ?> ,</strong><br>
							OWN CAMPUS :77, Satmasjid Road Dhanmondi,<br>
							Dhaka 1205,Bangladesh.<br>
							Hotline :16665,01766661555,<br>
							  Email: info@sub.edu.bd
							
					
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  
                  <address>
                    <strong>Event :<?php $dep_adm_count=mysqli_query($con,"select *from  event where dept_code='$dept_code'");
                                              			
                                       
          while($row=mysqli_fetch_assoc($dep_adm_count))){ echo $row['event_id'];
		                 $strttime=$row['start_time']; 
                          $enddate=$row['end_date'];
                             $endtime=$row['end_time']; $trgetparti=$row['target_participants'];	
							   $guest=$row['guest_allowed'];
							   } ?><br>
                    Start date:<?php  echo $event_date; ?><br>
                    Start Time:<?php  echo $strttime; ?><br>
					End Date:<?php  echo $enddate; ?><br>
					End Time:<?php  echo $endtime ;?><br>
					Guest allowed:<?php  if ($guest==1) {echo "Yes";}
if ($guest==0){echo "No";}					?><br>
				Target Participants: <?php if ($trgetparti==1){
			  echo "1.All";}
			  if ($trgetparti==2){ echo
			"2.only Students";}
			
			if ($trgetparti==3){ echo
			"3.Student,<br>Academic staff";}
			
			if ($trgetparti==4){ echo
			"4.Student,<br>Academic staff,Non_Academic staff";}
			
			if ($trgetparti==5){ echo
			"5.only academic Staff";}
			if ($trgetparti==6){ echo
			"6.academic staff,<br>non_academic staff";}
			if ($trgetparti==7){ echo
			"7.only non academic staff";}
			
			if ($trgetparti==8){ echo
			"8.Alumni";}
			
			if ($trgetparti==9){ echo
			"9.Student,<br>academic staff,alumni";}
			
			if ($trgetparti==10){ echo
			"10">"10.Alumni,<br>Academic staff+Non Academic_Staff";}
			
			
			
			?> <br>
			</strong>
                    
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>.....</b><br>
                  <br>
                  <b>....</b> ....<br>
                  <b></b> ////<br>
                  <b>.....</b> /////
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                     <div class="card">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
			   
                <!-- /.col -->
              </div>
                <table id="example1a" class="eventtb88">
				<h2> Details </h2>
			
                  <?php $res3 = mysqli_query($con, " SELECT *,SUM(amount_as_total) 
 FROM event_participants_registrations,payment_status
 WHERE event_participants_registrations.registration_id = payment_status.registration_id
 and event_id='$event_id' and payment_status='1'  and registration_status='1'
"); ?>
     <thead>         
   <tr>
   
   <!-- <th>S.r</th> 
   <th>Sr.no</th> -->
   <th>......Total Amount.tk.......</th>
   <th>......Total Participants......</th>
   
	<th>......Total Collections.....  </th>
	<th>............Total Guests...........</th>
	<th>... link and Use as any Notes...</th>
	
	
	


	
	</tr>
	</thead>
	
	
	<tbody>
	
	<?php
$i = 1;
while ($row = mysqli_fetch_assoc($res3))
{ ?>
	
	 <tr>
	 
	  
    
	   <td<?php   $dep_count=mysqli_query($con," SELECT *,SUM(amount_as_total) ,event.*
 FROM event_participants_registrations,payment_status ,event
 WHERE event_participants_registrations.registration_id = payment_status.registration_id 
         and event_participants_registrations.event_id = event.event_id
  and dept_code='$dept_code' and registration_status='1' and payment_status='1';
");
                                          /* while  ( $rows=mysqli_fetch_assoc($dep_count)){	
                                       // echo '<h4 style="olor:black'> Number of Departments : $rows['amoust_as_total']</h4>';
											echo $rows['SUM(amount_as_total)'];	 	}	*/		
										$rows=mysqli_fetch_assoc($dep_count);
											$amount_total=$rows['SUM(amount_as_total)'];
                                       // echo '<h4 style="olor:black'> Number of Departments : $rows['SUM(amount_as_total)']</h4>';
									echo  "<h4> Total: $amount_total </h4>";		?>td>
									
									
	    <td><?php	 		$total_user22=$total2sv+$total2av+$total33nav;	
                                        echo "<h4 > Active :  $total_user22</h4>"; ?> <br>
										<?php	 $count2=mysqli_query($con,"SELECT COUNT(registration_id),students.* 
										
                                                    FROM event_participants_registrations,students 
                                                 WHERE event_participants_registrations.user_id = students.user_id
        
                                             and dept_code='$dept_code' and registration_status='1'  ");
                                              $rows2=mysqli_fetch_assoc($count2);
											$total2sv=$rows2['COUNT(registration_id)'];
                                       // echo '<h4 style="olor:black'> Number of Departments : $rows['SUM(amount_as_total)']</h4>';
									echo  "<h4> Valided stude: $total2sv </h4>";	 ?> <br>
									<?php	 $count1=mysqli_query($con,"SELECT COUNT(registration_id),academic_staff.* 
										
 FROM event_participants_registrations,academic_staff 
 WHERE event_participants_registrations.user_id = academic_staff.user_id
        
  and dept_code='$dept_code' and registration_status='1'  ");
                                              $rows1=mysqli_fetch_assoc($count1);
											$total2av=$rows1['COUNT(registration_id)'];
                                      
									echo  "<h4> academic Valided: $total2av </h4>";	 ?> <br>
									<?php	 $count1=mysqli_query($con,"SELECT COUNT(registration_id),non_academic_staff.* 
										
 FROM event_participants_registrations,non_academic_staff 
 WHERE event_participants_registrations.user_id = non_academic_staff.user_id
        
  and dept_code='$dept_code' and registration_status='1'  ");
                                              $rows1=mysqli_fetch_assoc($count1);
											$total33nav=$rows1['COUNT(registration_id)'];
                                      
									echo  "<h4> Non academic Valided: $total33nav </h4>";	 ?>	
										
										</td>
	  
		   <td><?php	 $dep_adm_count=mysqli_query($con,"SELECT event_participants_registrations.*,collection_info.collection_status
 FROM event_participants_registrations,collection_info
 WHERE 
         event_participants_registrations.registration_id = collection_info.registration_id
  and dept_code='$dept_code' and registration_status='1' and collection_info.collection_status='1'");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Collected : $rows1sr </h6>"; ?><br>
										<?php	 $dep_adm_count=mysqli_query($con,"SELECT event_participants_registrations.*,collection_info.collection_status
 FROM event_participants_registrations,collection_info
 WHERE 
         event_participants_registrations.registration_id = collection_info.registration_id
  and dept_code='$dept_code' and registration_status='1' and collection_info.collection_status='0'");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Not Collected : $rows1sr </h6>"; ?></td>
		   <td><?php	 $dep_adm_count=mysqli_query($con,"select SUM(number_of_guest) from  event_participants_registrations where event_id='$event_id' and registration_status='1'");
                                               $row11=mysqli_fetch_assoc($dep_adm_count);
											   $guests=$row11['SUM(number_of_guest)'];
                                        echo "<h5 >Total : $guests  </h5>" ?></td>
		   <td> </td>
	
		
	
	</tr>

			

             
 <tr>
				   
                   
   <th>.......</th>
   <th>.......</th>
   
	<th>......</th>
	<th>.......</th>
	<th>...... </th>
	
	
	
                  </tr>
				 
			 
                  <tr>
				   
                   <th>
				  ..............
				   <address>
                    <strong>
							</strong><br>
					
                 
                  </address></th>
			 <th>    <strong>  </strong><br>
                   
                   
                </th>	
           <th><br>
                  <br>
				   <strong>
                 .....
                
				  </strong>
				  
           </th>
   <th></th>
  
	<th><?php $unilink="www.sub.ac.bd/?department=computer-science-and-engineering"; ?>
		   <img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=http%3A%2F%2F<?php echo $unilink?>%2F&choe=UTF-8" title="Link to Google.com" /></th>
	
                  </tr>
				  
				   
              <?php
    $i++;
} ?>
    
				 


				 </tbody>
				 
                </table>
              </div>
			
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
              
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">......</p>

                 
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
				<?php    $res1=mysqli_query($con,"select *from  event where event_id='$event_id'");
                                              						?>
																	
																	 <?php 
	 $i=1;
	while($row=mysqli_fetch_assoc($res1)){?>
	
	
                                       
	
	
      
		 <?php echo $row['event_id'];?>
	   
	     
		
			
			 <a href="invoice-print.php?event_id=<?php echo $row['event_id']?>" rel="noopener"  
			 class="btn btn-primary float-right"><i class="fas fa-download"></i>Download pdf or print</a>
	
	
	</tr>
 


 
   <?php 
	 $i++;
	} ?>
	
                  
                 
                 
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>