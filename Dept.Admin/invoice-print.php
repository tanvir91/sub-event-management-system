<?php

include('../db.php');

//session_start();
//echo $_SESSION['dept_adlogin'];
$user=$_SESSION['dept_adlogin'];
//include('db.php');
if(!isset($_SESSION['dept_adlogin'])) {
	header('location:../dept_admin_login.php');
	die();
}




$res=mysqli_query($con,"select *from  dept_admin where user_id='".$_SESSION['dept_adlogin']."'");
//$row=mysqli_fetch_assoc($res);

$active_user=mysqli_fetch_assoc($res);
$user_id=$active_user['user_id'];
$dept_code=$active_user['dept_code'];

 
 $res1=mysqli_query($con,"select dept_code from dept_admin  where dept_code='$dept_code'");
 $row=mysqli_fetch_assoc($res1);
 $recent=$row['dept_code'];


$event_id=mysqli_real_escape_string($con,$_GET['event_id']);


/*	if($event_id==''){
	header('location:deparment_admins.php.php');
	die(); 
	
}*/
$res2=mysqli_query($con,"select * from  event_participants_registrations where event_id='$event_id'");
$row1=mysqli_fetch_assoc($res2);



?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SUB | Invoice <?php echo $event_id ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <style>
  table, th,td {
  border: 1px solid black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: rgba(150, 212, 212, 0.4);
}



 th,td {
  padding-top: 2px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 15px;
  font-weight: bold;
  font-size: 12px;
  
  
}
th {
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 15px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 15px;
}
  
  </style>
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
   <section class="invoice">
    <!-- title row -->
   
    <!-- info row -->
    <div class="row invoice-info">

<?php $res1=mysqli_query($con,"select *from  event where event_id='$event_id'");
          while($row=mysqli_fetch_assoc($res1)){$row['event_id'];} ?>
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class=""><img src="../image/download.png" alt="Visa" width="150" height="50"></i>
					
                    <small class="float-right"><?php date_default_timezone_set('Asia/Dhaka');
					echo $date = date('m/d/Y h:i:s a', time());
					 ?></small>
					<br><br>Event Invoice:<?php $res1=mysqli_query($con,"select *from  event where event_id='$event_id'");
          while($row=mysqli_fetch_assoc($res1)){ echo $row['event_id'];
		  $event_date=$row['start_date'];} ?>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  
                   <address>
                    <strong>State University of Bangladesh.<br>Dept. of <?php echo $dept_code;  ?> ,</strong><br>
							OWN CAMPUS :77, Satmasjid Road Dhanmondi,<br>
							Dhaka 1205,Bangladesh.<br>
							Hotline :16665,01766661555,<br>
							  Email: info@sub.edu.bd
							
					
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  
                  <address>
                    <strong>Event :<?php $res1=mysqli_query($con,"select *from  event where event_id='$event_id'");
          while($row=mysqli_fetch_assoc($res1)){ echo $row['event_id'];
		                 $strttime=$row['start_time']; 
                          $enddate=$row['end_date'];
                             $endtime=$row['end_time']; $trgetparti=$row['target_participants'];	
							   $guest=$row['guest_allowed'];
							   } ?><br>
                    Start date:<?php  echo $event_date; ?><br>
                    Start Time:<?php  echo $strttime; ?><br>
					End Date:<?php  echo $enddate; ?><br>
					End Time:<?php  echo $endtime ;?><br>
					Guest allowed:<?php  if ($guest==1) {echo "Yes";}
if ($guest==0){echo "No";}					?><br>
				Target Participants: <?php if ($trgetparti==1){
			  echo "1.All";}
			  if ($trgetparti==2){ echo
			"2.only Students";}
			
			if ($trgetparti==3){ echo
			"3.Student,<br>Academic staff";}
			
			if ($trgetparti==4){ echo
			"4.Student,<br>Academic staff,Non_Academic staff";}
			
			if ($trgetparti==5){ echo
			"5.only academic Staff";}
			if ($trgetparti==6){ echo
			"6.academic staff,<br>non_academic staff";}
			if ($trgetparti==7){ echo
			"7.only non academic staff";}
			
			if ($trgetparti==8){ echo
			"8.Alumni";}
			
			if ($trgetparti==9){ echo
			"9.Student,<br>academic staff,alumni";}
			
			if ($trgetparti==10){ echo
			"10">"10.Alumni,<br>Academic staff+Non Academic_Staff";}
			
			
			
			?> <br>
			</strong>
                    
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>.....</b><br>
                  <br>
                  <b>....</b> ....<br>
                  <b></b> ////<br>
                  <b>.....</b> /////
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                     <div class="card">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
			   
                <!-- /.col -->
              </div>
                <table id="example1" class="eventtb88">
				<h2> Details </h2>
			
                  <?php $res3 = mysqli_query($con, " SELECT *,SUM(amount_as_total) 
 FROM event_participants_registrations,payment_status
 WHERE event_participants_registrations.registration_id = payment_status.registration_id
 and event_id='$event_id' 
"); ?>
     <thead>         
   <tr>
   
   <!-- <th>S.r</th> 
   <th>Sr.no</th> -->
   <th>......Total Amount........</th>
   <th>......Total participants......</th>
   
	<th>......Total Collections Info.....  </th>
	<th> .....Total Payment Status....</th>
	<th>Total guests</th>
	<th>Total Volunteer</th>
	<th>... link and Use as any Notes...</th>
	
	
	


	
	</tr>
	</thead>
	
	
	<tbody>
	
	<?php
$i = 1;
while ($row = mysqli_fetch_assoc($res3))
{ ?>
	
	 <tr>
	 
	  
    <!--   <td><?php echo $i ?></td>  -->
	   <td><?php   $dep_count=mysqli_query($con," SELECT *,SUM(amount_as_total) 
 FROM event_participants_registrations,payment_status
 WHERE event_participants_registrations.registration_id = payment_status.registration_id
 and event_id='$event_id' and payment_status='1'  and registration_status='1'
");
                                            $rows=mysqli_fetch_assoc($dep_count);
											$amount_total=$rows['SUM(amount_as_total)'];
                                       // echo '<h4 style="olor:black'> Number of Departments : $rows['SUM(amount_as_total)']</h4>';
									echo  "<h4> Total: $amount_total </h4>";		 					   ?></td>
	    <td><?php	 $dep_adm_count=mysqli_query($con,"select *from  event_participants_registrations where event_id='$event_id' and registration_status='1'");
                                               $rows=mysqli_num_rows($dep_adm_count);			
                                        echo "<h4 > Participants : $rows</h4>"; ?></td>
	  
		   <td><?php	 $dep_adm_count=mysqli_query($con,"SELECT event_participants_registrations.*,collection_info.collection_status
 FROM event_participants_registrations,collection_info
 WHERE 
         event_participants_registrations.registration_id = collection_info.registration_id
  and event_id='$event_id' and registration_status='1' and collection_info.collection_status='1'");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Collected : $rows1sr </h6>"; ?><?php	 $dep_adm_count=mysqli_query($con,"SELECT event_participants_registrations.*,collection_info.collection_status
 FROM event_participants_registrations,collection_info
 WHERE 
         event_participants_registrations.registration_id = collection_info.registration_id
  and event_id='$event_id' and registration_status='1' and collection_info.collection_status='0'");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Not Collected : $rows1sr </h6>"; ?></td>
										
										 <td><?php	 $dep_adm_count=mysqli_query($con,"SELECT event_participants_registrations.*,payment_status.payment_status
 FROM event_participants_registrations,payment_status
 WHERE 
         event_participants_registrations.registration_id = payment_status.registration_id
  and event_id='$event_id' and registration_status='1' and payment_status.payment_status='1'");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Verified: $rows1sr </h6>"; ?><?php	
		$dep_adm_count=mysqli_query($con,"SELECT event_participants_registrations.*,payment_status.payment_status
 FROM event_participants_registrations,payment_status
 WHERE 
         event_participants_registrations.registration_id =payment_status.registration_id
  and event_id='$event_id' and registration_status='1' and payment_status.payment_status='0'");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Not Verified : $rows1sr </h6>"; ?></td>
		   <td><?php	 $dep_adm_count=mysqli_query($con,"select SUM(number_of_guest) from  event_participants_registrations where event_id='$event_id' and registration_status='1'");
                                               $row11=mysqli_fetch_assoc($dep_adm_count);
											   $guests=$row11['SUM(number_of_guest)'];
                                        echo "<h5 >Total : $guests  </h5>" ?></td>
										<td><?php	 $dep_adm_count=mysqli_query($con,"SELECT volunteer.*,assigned_events_volunteer.*,event.*
 FROM volunteer,assigned_events_volunteer,event
 WHERE 
         assigned_events_volunteer.event_id = event.event_id and volunteer.user_id=assigned_events_volunteer.user_id
  and event.event_id='$event_id' ");
                                               $rows1sr=mysqli_num_rows($dep_adm_count);			
                                        echo "<h6 > Total: $rows1sr </h6>"; ?></td>
		   <td> </td>
		     
		
	
	</tr>

			

             
 <tr>
				   
                   
   <th>.......</th>
   <th>.......</th>
   
	<th>......</th>
	<th>.......</th>
	<th>...... </th>
	<th>...... </th>
	<th>...... </th>
	
	
	
                  </tr>
				 
			 
                  <tr>
				   
                   <th>
				   .......
				   <address>
                    <strong>
							</strong><br>
					
                 
                  </address></th>
			 <th>    <strong>  </strong><br>
                   
                   
                </th>	
   <th><br>
                  <br>
				   <strong>
                 .....
                
				  </strong>
				  
                 </th>
   <th></th>
  <th>...... </th>
	       <th>
		   </th>
		    <th>
		   </th>
	
                  </tr>
				  
				   
              <?php
    $i++;
} ?>
    
				 


				 </tbody>
				 
                </table>
              </div>
			
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
              
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">.....</p>

                 
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
             
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      <!-- /.container-fluid -->
    </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
<!-- Page specific script -->
<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>
