<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  
    <link rel="stylesheet" href="../https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  
  
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
      <link href="css/styles.css" rel="stylesheet" /> 
        
		 
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"  />
	 
	 
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
	<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css"  />
 <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css"  /> 
 


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css"  />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css"  />
	  
	<!-- https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css
https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css
https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css -->
  
 
 
  <!--<link rel="stylesheet" type="text/css "href="css/style.css">
<link rel="stylesheet" type="text/css "href="css/calorsol.css"> 
<link rel="stylesheet" type="text/css "href="css/responsive.bootstrap4.css"> 
<link rel="stylesheet" type="text/css "href="css/responsive.bootstrap4.min.css"> 
<link rel="stylesheet" type="text/css "href="css/responsive.dataTables.css"> 
<link rel="stylesheet" type="text/css "href="css/responsive.dataTables.min.css">-->

<link href="https://fonts.googleapis.com/css2?family=Ibarra+Real+Nova&family=Josefin+Sans:wght@100&display=swap" rel="stylesheet">

  <style>	
	
	
	
		table, th,td {
  border: 1px solid black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: rgba(150, 212, 212, 0.4);
}



 th,td {
  padding-top: 2px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 15px;
  font-weight: bold;
  font-size: 15px;
  
  
}
th {
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 15px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 18px;
  
  
}
.eventtb88 th, .eventtb88 td {
  padding-top: 8px;
  padding-bottom: 3px;
  padding-left: 1px;
  padding-right: 6px;
  font-weight: bold;
  font-size: 12px;
  
  
}
.eventtb88 th {
  padding-top: 8px;
  padding-bottom: 3px;
  padding-left: 1px;
  padding-right: 6px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 11px;
  
  
}

.ss th, .ss td {
  padding-top: 8px;
  padding-bottom: 6px;
  padding-left: 1px;
  padding-right: 10px;
  font-weight: bold;
  font-size: 17px;
  
  
}
.ss th {
  padding-top: 8px;
  padding-bottom: 6px;
  padding-left: 1px;
  padding-right: 10px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 18px;
  
  
}



a.view_part:link, a.view_part:visited {
  background-color: #92196c;
  color: black;
  padding: 6px 8px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.view_part:hover, a.view_part:active {
  background-color: blue;
}


a.upadmin:link, a.upadmin:visited {
  background-color: #199249;
  color: black;
  padding: 8px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
}

a.upadmin:hover, a.upadmin:active {
  background-color: blue;
  font-size: 14px;
}

a.get_back:link, a.get_back:visited {
  background-color: #199249;
  color: black;
  padding: 10px 14px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
}

a.get_back:hover, a.get_back:active {
  background-color: blue;
  font-size: 14px;
}

a.event_assign:link, a.event_assign:visited {
  background-color: #199249;
  color: black;
  padding: 12px 18px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 22px;
}

a.event_assign:hover, a.event_assign:active {
  background-color: blue;
  font-size: 22px;
}


a.up11pass:link, a.up11pass:visited {
  background-color: #7ede09;
  color: black;
  padding: 8px 9px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
}

a.up11pass:hover, a.up11pass:active {
  background-color: blue;
  font-size: 14px;
}

a.dlt:link, a.dl_dp_admin:visited {
  background-color: #FF0000;
  color: black;
  padding: 6px 12px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.dlt:hover, a.dlt:active {
  background-color: blue;
}





a.nav-link:hover, a.nav-link:active {
  background-color: green;
  color: black;
}
</style>
 
</head>
<body>




<!-- nav Start-->
 
<!-- nav close-->


<!--  carosal-->


<nav class="navbar navbar-expand-lg navbar-dark bg-success">

 <a class="navbar-brand" href="#">
      <img src="../image\download.png" alt="" width="200" height="70" class="d-inline-block align-text-top">
     
    </a>
    <a class="navbar-brand" href="#">SUB EMS</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>





  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
	
	 <li class="nav-item active">
        <a class="nav-link" href="#"><h5  >Dept. of  <?php echo $dept_code;?></h5> </a>
		
      </li> 
      <li class="nav-item active">
        <a class="nav-link" href="4volunteer_user_dashboard.php">HOME<span class="sr-only">(current)</span></a>
      </li>


       

      <!--    <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          DEPARTMENT
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="BBA.html">BBA</a>
          <a class="dropdown-item" href="#">CSE</a>
           <a class="dropdown-item" href="#">LAW</a>
           <a class="dropdown-item" href="#">Food</a>
            <a class="dropdown-item" href="#">Pharmacy</a>
            <a class="dropdown-item" href="#">ARCHITECTURE</a>
             <a class="dropdown-item" href="#">English Studies</a>

</div>
          </li> -->
        <li class="nav-item active">
        <a class="nav-link" href="assigned_events_for_vol.php">ASSIGNED EVENTS</a>
      </li>   
     

      


      <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          PROFILE
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		<a class="dropdown-item" href="#"> <?php echo $user_id?></a>
		
          
		  <a class="dropdown-item" href="Change_pass.php">Change Password</a>
          
           
          

</div>
         </li>
		 
		    <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="4#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
          LOGOUT
        </a> 
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		
          <a class="dropdown-item" href="4volunteer_user_logout.php">Logout</a>
           
          

</div>
        
</li>

    </ul>
    
  </div>
</nav>

<div id="layoutSidenav_content">
                <main>

<div class="container-fluid px-4">

  <!--<div class="card mb-4">
   <div class="card-header">
                                
    <h1 style="color:#3034B0;" >Dept. of  <?php echo $dept_code;?></h1>                     
   <h2 style="color:#3034B0;">Hello : <?php echo  $user_id; ?>, </h2>
   <h2 style="color:#3034B0;"> Volunteer</h2>
  
  </div> 
  </div> -->