-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2022 at 01:44 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newp_dib`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_staff`
--

CREATE TABLE `academic_staff` (
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `academic_staff`
--

INSERT INTO `academic_staff` (`user_id`, `password`, `dept_code`, `user_type`, `first_name`, `last_name`, `email`, `contact_no`, `gender`, `status`) VALUES
('ACA-CSE-001', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Academic_Staff', 'Muntasir Hasan', 'Kanchan', 'muntasir@sub.edu.bd', '+01788', 'Male', 1),
('ACA-CSE-002', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Academic_Staff', 'Mamoon Al', 'Rashid', 'thp2p@vowg.com', '0462143105', 'Male', 1),
('ACA-CSE-003', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Academic_Staff', 'Partho', 'Voumic', 'kielc@spap.com', '6219937129', 'Male', 1),
('ACA-PHA-001', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Academic_Staff', 'Nadri', 'lal', 'fiktm@tuwd.com', '7935197958', 'Male', 1),
('ACA-PHA-002', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Academic_Staff', 'Lal', 'Mohon', '7ep3u@bqcs.com', '4600788240', 'Male', 1),
('ACA-PHA-003', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Academic_Staff', 'Joynal', 'Rahman', 'ah4w7@apif.com', '7372139576', 'Male', 1);

-- --------------------------------------------------------

--
-- Table structure for table `assigned_events_volunteer`
--

CREATE TABLE `assigned_events_volunteer` (
  `assigned_id` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `event_id` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status2` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assigned_events_volunteer`
--

INSERT INTO `assigned_events_volunteer` (`assigned_id`, `user_id`, `event_id`, `status`, `status2`) VALUES
(25, 'UG02-36-14-009', 'Annual_pinic_2022', 1, 0),
(28, 'UG02-38-18-01', 'Annual_pinic_2022', 1, 0),
(29, 'UG02-36-14-009', 'convocation', 1, 0),
(30, 'UG02-53-18-009', 'Programming_contest', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `collection_info`
--

CREATE TABLE `collection_info` (
  `registration_id` int(255) NOT NULL,
  `ticket` tinyint(1) NOT NULL,
  `kit` tinyint(1) NOT NULL,
  `coupon` tinyint(1) NOT NULL,
  `attendance` tinyint(1) NOT NULL,
  `collection_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `collection_info`
--

INSERT INTO `collection_info` (`registration_id`, `ticket`, `kit`, `coupon`, `attendance`, `collection_status`) VALUES
(148, 0, 0, 0, 0, 0),
(149, 1, 1, 1, 1, 1),
(154, 0, 1, 1, 1, 0),
(157, 0, 0, 0, 0, 0),
(158, 0, 0, 0, 0, 0),
(159, 0, 0, 0, 0, 0),
(160, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dept_code` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `dept_status` tinyint(1) NOT NULL,
  `target_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_code`, `dept_name`, `dept_status`, `target_status`) VALUES
('ARCH', 'Dept.of Archetecture', 1, 0),
('BBA', 'Dept.of BBA', 1, 0),
('CSE', 'Dept. of CSE', 1, 0),
('ENG', 'Dept. of English', 1, 0),
('ENV', 'Dept. of Enviroment', 1, 0),
('FOOD', 'Dept. of Food', 1, 0),
('HEALTH', 'Dept.of Health', 1, 0),
('JOUR', 'Dept.of Jouralism', 1, 0),
('LAW', 'Dept. of Law', 1, 0),
('PHA', 'Dept.of Pharmacy', 1, 0),
('Registrar_office', 'SUB Registrar', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dept_admin`
--

CREATE TABLE `dept_admin` (
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dept_admin`
--

INSERT INTO `dept_admin` (`user_id`, `password`, `dept_code`, `user_type`, `first_name`, `last_name`, `email`, `contact_no`, `gender`, `status`) VALUES
('ARCH_ADMIN_005', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Department_Admin', 'Fahima', 'Salam', 'fahima@gmail.com', '+89000889', 'Female', 1),
('BBA_ADMIN_004', '81dc9bdb52d04dc20036dbd8313ed055', 'BBA', 'Department_Admin', 'Hasibul', 'Islam', 'hasibul@mail.com', '+880012', 'Male', 1),
('CSE_ADMIN_01', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Department_Admin', 'Muhammad Masud', 'Tarek', 'tarek@sub.edu.bd', '+880170', 'Male', 1),
('ENG-ADMIN-07', '81dc9bdb52d04dc20036dbd8313ed055', 'ENG', 'Department_Admin', 'Sadat', 'Hasan', 'hasan@mail.com', '+8801', 'Male', 1),
('ENV-ADMIN-011', '81dc9bdb52d04dc20036dbd8313ed055', 'ENV', 'Department_Admin', 'Md. Zahidul ', 'Islam', 'islam@mail.com', '+888', 'Male', 1),
('FOOD_ADMIN_03', '81dc9bdb52d04dc20036dbd8313ed055', 'FOOD', 'Department_Admin', 'Manobendro', ' Sarker', 'Sarker@mail.com', '+09988', 'Male', 1),
('HEALT-ADMIN-06', '81dc9bdb52d04dc20036dbd8313ed055', 'HEALTH', 'Department_Admin', 'Dr. Humaira ', 'Tul Jannat', 'humaira@mail.com', '+880112', 'Female', 1),
('JOU_ADMIN_08', '81dc9bdb52d04dc20036dbd8313ed055', 'JOUR', 'Department_Admin', 'Sohel ', 'Manzur', 'manzur@gmail.com', '+881221', 'Male', 1),
('LAW_ADMIN_02', '81dc9bdb52d04dc20036dbd8313ed055', 'LAW', 'Department_Admin', 'Sazzad', 'Rohan', 'rehan@gmail.com', '+8801643', 'Male', 1),
('PHA-ADMIN-010', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Department_Admin', 'Saimon', 'Shahriar', 'shahriar@mail.com', '+881212', 'Male', 1),
('REG_admin', '81dc9bdb52d04dc20036dbd8313ed055', 'Registrar_office', 'NON_Academic', 'Ahsan', 'Ali', 'aop@mail.com', '+00212', 'Male', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` varchar(255) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `registration_fee` double NOT NULL,
  `payment_required` tinyint(1) NOT NULL,
  `guest_allowed` tinyint(1) NOT NULL,
  `target_participants` tinyint(2) NOT NULL,
  `target_dept` tinyint(1) NOT NULL,
  `registration_enable` tinyint(1) NOT NULL,
  `event_status` tinyint(1) NOT NULL,
  `details` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `fb_link` varchar(255) NOT NULL,
  `url_link` varchar(255) NOT NULL,
  `notice_link` varchar(255) NOT NULL,
  `link_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_name`, `dept_code`, `start_date`, `start_time`, `end_date`, `end_time`, `registration_fee`, `payment_required`, `guest_allowed`, `target_participants`, `target_dept`, `registration_enable`, `event_status`, `details`, `image`, `fb_link`, `url_link`, `notice_link`, `link_status`) VALUES
('Annual_pinic_2022', 'CSE Annual Picnic 2022', 'CSE', '2022-03-30', '07:00:00', '2022-03-30', '21:30:00', 1000, 1, 1, 1, 0, 1, 1, 'this is going to be.....', 'sub.picnic.jpg', 'https://www.facebook.com/', 'https://www.sub.ac.bd/?department=computer-science-and-engineering', 'https://www.sub.ac.bd/?department=computer-science-and-engineering', 1),
('convocation', 'Sub Convcation 2022', 'Registrar_office', '2022-05-07', '01:29:00', '2022-07-09', '07:24:00', 5000, 1, 1, 1, 1, 1, 1, '', 'download.jpeg', 'https://www.facebook.com/', 'https://www.facebook.com/', 'https://www.facebook.com/', 1),
('Ifter_Mahfil_2022', 'SUB Ifter Mahfil ', 'Registrar_office', '2022-04-05', '23:16:00', '2022-04-04', '17:10:00', 0, 1, 1, 1, 0, 1, 0, '', 'nnn.jpg', 'https://www.facebook.com', 'https://www.facebook.com', 'https://www.facebook.com', 1),
('Pharma_Week_22', 'Pharmacy week 2022', 'PHA', '2022-03-25', '07:30:00', '2022-03-26', '18:40:00', 500, 1, 1, 4, 0, 1, 1, '					  The Department of Pharmacy, State University of Bangladesh (SUB) is one of the pioneering departments in the private sector, which is offering Pharmacy education in Bangladesh due to its high-quality teaching and excellence in research. The department has been delivering world-class education since 2004 by ensuring outstanding teaching by highly qualified and experienced faculty members, extensive hands on demonstration in the laboratories and innovative world class research activities. The department aims to develop the students’ research capacity through its well-equipped and advanced laboratories and technical knowledge concerning the recent and up-to-date developments in pharmaceutical sciences. In 2021, the faculty members of the department have published 60 research articles in internationally recognized peer-reviewed and top-ranking scientific journals. The Scopus, Web of Science and PubMed databases have indexed a substantial number of the published papers and several articles have					  					  					  ', 'parma.jpg', 'https://www.facebook.com/subedubd', 'https://www.sub.ac.bd/?department=pharmacy', 'https://www.sub.ac.bd/?department=pharmacy', 1),
('Programming_contest', 'asdas', 'CSE', '2022-03-23', '04:31:00', '2022-03-23', '18:37:00', 122, 1, 1, 2, 0, 1, 1, '', 'slider_3.jpg', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event_participants_registrations`
--

CREATE TABLE `event_participants_registrations` (
  `registration_id` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `event_id` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `number_of_guest` varchar(255) NOT NULL,
  `transection_id` varchar(255) NOT NULL,
  `registration_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_participants_registrations`
--

INSERT INTO `event_participants_registrations` (`registration_id`, `user_id`, `event_id`, `user_type`, `number_of_guest`, `transection_id`, `registration_status`) VALUES
(148, 'UG02-38-18-009', 'Programming_contest', 'Student', '0', 'asdasd', 1),
(149, 'UG02-38-18-009', 'Annual_pinic_2022', 'Student', '1', 'sasdasd', 1),
(154, 'ACA-CSE-003', 'Annual_pinic_2022', 'Academic_Staff', '1', 'asdasdasd', 1),
(157, 'NON-A-CSE-007', 'Annual_pinic_2022', 'NON_Academic', '0', 'sadasd', 1),
(158, 'UG02-38-18-009', 'convocation', 'Student', '2', 'cvxzz', 1),
(159, 'ACA-CSE-003', 'convocation', 'Academic_Staff', '1', 'zxxzxzx', 1),
(160, 'UG02-37-14-011', 'convocation', 'Student', '1', 'bccc', 0);

-- --------------------------------------------------------

--
-- Table structure for table `non_academic_staff`
--

CREATE TABLE `non_academic_staff` (
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `non_academic_staff`
--

INSERT INTO `non_academic_staff` (`user_id`, `password`, `dept_code`, `user_type`, `first_name`, `last_name`, `email`, `contact_no`, `gender`, `status`) VALUES
('NON-A-CSE-004', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'NON_Academic', 'Amin', 'Ali', 'uuuxs@ulds.com', '5330967946', 'Male', 1),
('NON-A-CSE-007', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'NON_Academic', 'Moin', 'Haseen', 'kqffy@tzoz.com', '1596799737', 'Male', 1),
('NON-A-CSE-11', '24dfb9e4d666a16b3ae340b65232ec47', 'CSE', 'NON_Academic', 'Zakir', 'Hossain', 'zxczxc@gmail.com', '2323', 'Male', 1),
('NON-A-PHA-001', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'NON_Academic', 'Jashim', 'Ali', 'vy6n8@lnig.com', '2024414553', 'Male', 1),
('NON-A-PHA-002', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'NON_Academic', 'Maryan', 'lia', 'ge2u4@xzf9.com', '7299926412', 'Male', 1),
('NON-A-PHA-004', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'NON_Academic', 'Rahmat', 'Ali', 'zvv1l@3d7d.com', '4592906962', 'Male', 1),
('NON-A-PHA-005', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'NON_Academic', 'Sadek', 'Khan', 'v3vnr@irgp.com', '8254449732', 'Male', 1),
('NON_A_CSE_001', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'NON_Academic', 'Kaniz ', 'Farhana', 'kaniz@sub.edu.bd', '+880167767', 'Female', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

CREATE TABLE `payment_status` (
  `registration_id` int(255) NOT NULL,
  `transection_id` varchar(255) NOT NULL,
  `amount_as_total` double NOT NULL,
  `payment_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`registration_id`, `transection_id`, `amount_as_total`, `payment_status`) VALUES
(148, 'asdasd', 122, 1),
(149, 'sasdasd', 2000, 1),
(154, 'asdasdasd', 2000, 1),
(157, 'sadasd', 1000, 1),
(158, 'cvxzz', 15000, 1),
(159, 'zxxzxzx', 10000, 1),
(160, 'bccc', 10000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `alumni_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`user_id`, `password`, `dept_code`, `user_type`, `first_name`, `last_name`, `email`, `contact_no`, `gender`, `status`, `alumni_status`) VALUES
('UG02-36-14-009', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'Tanvir', 'Hossain', 'htanvir888@gmail.com', '+88011', 'Male', 1, 0),
('UG02-37-14-011', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'Mahmud ', 'Hasan', 'mahamudbd93@gmail.com', '+8801732', 'Male', 1, 0),
('UG02-37-14-012', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'Rohan ', 'Raja', 'raja@mail.com', '+0990', 'Male', 1, 0),
('UG02-38-18-009', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'zara', 'Ahsan', 'zara@mail.com', '+09912', 'Female', 1, 0),
('UG02-38-18-01', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'Kareem', 'Mia', 'v94oc@odcq.com', '6630915938', 'Male', 1, 0),
('UG02-38-18-09', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'Jabed', 'Pathoary', 'jshrs@1h4x.com', '0818312540', 'Male', 1, 0),
('UG02-38-18-0sss09', '81dc9bdb52d04dc20036dbd8313ed055', 'LAW', 'Student', 'asas', 'assaas', 'asasdsa@gmail.com', '222', 'Male', 0, 1),
('UG02-53-18-009', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', 'Student', 'Arif', 'Arif', 'arif@gmail.com', 'arif@mail.com', 'Male', 1, 0),
('UGARCH-38-18-03', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Latif', 'Ali', 'ali@mail.com', '09023', 'Male', 1, 0),
('UGARCH-38-18-04', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Simanto', 'Adi', 'djiasjd@mail.com', '+92032', 'Male', 1, 0),
('UGARCH-38-18-05', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Milon', 'Sarkar', 'spaam@p3hy.com', '5135305871', 'Male', 1, 0),
('UGARCH-38-18-06', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Tarek', 'Aziz', 't1sjr@yoji.com', '3564637505', 'Male', 1, 1),
('UGARCH-38-18-07', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Firoz', 'Khan', 'jb0y6@wr4m.com', '0252370762', 'Female', 1, 0),
('UGARCH-38-18-08', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Liakot', 'Shikdar', 'c0po7@fypj.com', '8982107457', 'Male', 1, 0),
('UGARCH-38-18-09', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', 'Student', 'Ratul', 'Topu', '86rep@zoog.com', '3125595714', 'Male', 1, 0),
('UGJOU-38-18-01', '81dc9bdb52d04dc20036dbd8313ed055', 'JOUR', 'Student', 'Tafsir', 'Rahman', '7t6mg@x3wc.com', '3018664225', 'Male', 1, 0),
('UGJOU-38-18-02', '81dc9bdb52d04dc20036dbd8313ed055', 'JOUR', 'Student', 'Ronojit', 'Mahan', 'e6ffo@cfw9.com', '5730997648', 'Male', 1, 0),
('UGJOU-38-18-03', '81dc9bdb52d04dc20036dbd8313ed055', 'JOUR', 'Student', 'Zaman', 'Khan', '02zau@9ebr.com', '1199829096', 'Male', 1, 0),
('UGJOU-38-18-04', '81dc9bdb52d04dc20036dbd8313ed055', 'JOUR', 'Student', 'Shoumic', 'Raj', 'n57ww@hwst.com', '5202411183', 'Male', 1, 0),
('UGJOU-38-18-05', '81dc9bdb52d04dc20036dbd8313ed055', 'JOUR', 'Student', 'Pranto', 'Azad', 'ghsxs@wfdy.com', '1969881969', 'Male', 1, 0),
('UGLAW-38-18-009', '81dc9bdb52d04dc20036dbd8313ed055', 'LAW', 'Student', 'RAJA', 'KARIM', 'ajsd@gmail.com', '+8801483', 'Male', 1, 0),
('UGLAW-38-18-010', '81dc9bdb52d04dc20036dbd8313ed055', 'LAW', 'Student', 'Rahat', 'Arif', 'arif@mail.com', '+8801221', 'Male', 1, 0),
('UGLAW-38-18-011', '81dc9bdb52d04dc20036dbd8313ed055', 'LAW', 'Student', 'Rony', 'Rana', 'rana@mail.com', '+89898', 'Male', 1, 0),
('UGPHA-38-18-01', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Student', 'piyash', 'Rony', 'asasd@gmail.com', '+889898', 'Male', 1, 0),
('UGPHA-38-18-011', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Student', 'Rina', 'karim', 'jinxz@mail.com', '+08989', 'Female', 1, 0),
('UGPHA-38-18-02', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Student', 'Raju', 'Riaj', 'ihad@mail.com', '+909932', 'Male', 1, 0),
('UGPHA-38-18-03', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', 'Student', 'Nishat', 'Raza', 'saha@mail.com', 'asdasd@mail.com', 'Male', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE `super_admin` (
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`user_id`, `password`, `dept_code`, `user_type`, `first_name`, `last_name`, `email`, `gender`, `status`) VALUES
('su_admin_01', '1234', 'IT', 'non-academic', 'zero', 'code', 'cg2222mail@gmail.com', 'm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `volunteer`
--

CREATE TABLE `volunteer` (
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `pass_to_work` varchar(255) NOT NULL,
  `recent_type` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `for_update` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `volunteer`
--

INSERT INTO `volunteer` (`user_id`, `password`, `dept_code`, `pass_to_work`, `recent_type`, `status`, `for_update`) VALUES
('UG02-36-14-009', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', '4321zz', 'volunteer', 1, 0),
('UG02-37-14-011', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', '4321', 'volunteer', 1, 0),
('UG02-37-14-012', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', '4321', 'volunteer', 1, 0),
('UG02-38-18-009', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', '4321', 'volunteer', 1, 0),
('UG02-38-18-01', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', '4321', 'volunteer', 1, 0),
('UG02-53-18-009', '81dc9bdb52d04dc20036dbd8313ed055', 'CSE', '4321', 'volunteer', 1, 0),
('UGARCH-38-18-05', '81dc9bdb52d04dc20036dbd8313ed055', 'ARCH', '4321', 'volunteer', 1, 0),
('UGLAW-38-18-009', '81dc9bdb52d04dc20036dbd8313ed055', 'LAW', '4321', 'volunteer', 1, 0),
('UGPHA-38-18-01', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', '4321', 'volunteer', 1, 0),
('UGPHA-38-18-011', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', '4321', 'volunteer', 1, 0),
('UGPHA-38-18-03', '81dc9bdb52d04dc20036dbd8313ed055', 'PHA', '4321', 'volunteer', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_staff`
--
ALTER TABLE `academic_staff`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `dept_code` (`dept_code`);

--
-- Indexes for table `assigned_events_volunteer`
--
ALTER TABLE `assigned_events_volunteer`
  ADD PRIMARY KEY (`assigned_id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `collection_info`
--
ALTER TABLE `collection_info`
  ADD PRIMARY KEY (`registration_id`),
  ADD KEY `registration_id` (`registration_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dept_code`);

--
-- Indexes for table `dept_admin`
--
ALTER TABLE `dept_admin`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `dept_code` (`dept_code`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`),
  ADD UNIQUE KEY `event_name` (`event_name`),
  ADD KEY `dept_code` (`dept_code`);

--
-- Indexes for table `event_participants_registrations`
--
ALTER TABLE `event_participants_registrations`
  ADD PRIMARY KEY (`registration_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `non_academic_staff`
--
ALTER TABLE `non_academic_staff`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `dept_code` (`dept_code`);

--
-- Indexes for table `payment_status`
--
ALTER TABLE `payment_status`
  ADD PRIMARY KEY (`registration_id`),
  ADD KEY `resgistration_id` (`registration_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `dept_code` (`dept_code`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `dept_code` (`dept_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_events_volunteer`
--
ALTER TABLE `assigned_events_volunteer`
  MODIFY `assigned_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `event_participants_registrations`
--
ALTER TABLE `event_participants_registrations`
  MODIFY `registration_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `academic_staff`
--
ALTER TABLE `academic_staff`
  ADD CONSTRAINT `academic_staff_ibfk_1` FOREIGN KEY (`dept_code`) REFERENCES `department` (`dept_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `assigned_events_volunteer`
--
ALTER TABLE `assigned_events_volunteer`
  ADD CONSTRAINT `assigned_events_volunteer_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assigned_events_volunteer_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `volunteer` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `collection_info`
--
ALTER TABLE `collection_info`
  ADD CONSTRAINT `collection_info_ibfk_1` FOREIGN KEY (`registration_id`) REFERENCES `event_participants_registrations` (`registration_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dept_admin`
--
ALTER TABLE `dept_admin`
  ADD CONSTRAINT `dept_admin_ibfk_1` FOREIGN KEY (`dept_code`) REFERENCES `department` (`dept_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`dept_code`) REFERENCES `department` (`dept_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `event_participants_registrations`
--
ALTER TABLE `event_participants_registrations`
  ADD CONSTRAINT `event_participants_registrations_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `non_academic_staff`
--
ALTER TABLE `non_academic_staff`
  ADD CONSTRAINT `non_academic_staff_ibfk_1` FOREIGN KEY (`dept_code`) REFERENCES `department` (`dept_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payment_status`
--
ALTER TABLE `payment_status`
  ADD CONSTRAINT `payment_status_ibfk_1` FOREIGN KEY (`registration_id`) REFERENCES `event_participants_registrations` (`registration_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`dept_code`) REFERENCES `department` (`dept_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD CONSTRAINT `volunteer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `students` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
