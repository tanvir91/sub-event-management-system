
                   
					
					
				<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
		
		 <link href="../css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css"  />
	 
	  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap4.min.css"  />
		  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        
      
	 
	 
	   
      
	  
  



  
  
  
        
   
        
    <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script> -->
	<style>	
	
	
	
			table, th,td {
  border: 1px solid black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: rgba(150, 212, 212, 0.4);
}



 th,td {
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 10px;
  font-weight: bold;
  font-size: 11px;
  
  
}
th {
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 1px;
  padding-right: 10px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 11px;
  
  
}

.place th, .place td {
  padding-top: 8px;
  padding-bottom: 3px;
  padding-left: 1px;
  padding-right: 18px;
  font-weight: bold;
  font-size: 15px;
  
  
}
.place th {
  padding-top: 8px;
  padding-bottom: 3px;
  padding-left: 1px;
  padding-right: 18px;
   background-color: #a7e810;
  color: black;
  font-weight: bold;
  font-size: 15px;
  
  
}


a.more:link, a.more:visited {
  background-color: #f44336;
  color: white;
  padding: 4px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.more:hover, a.more:active {
  background-color: red;
}

a.edit1:link, a.more1:visited {
  background-color: #339CFF;
  color: white;
  padding: 4px 12px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.edit1:hover, a.more1:active {
  background-color: blue;
}

a.add1:link, a.add1:visited {
  background-color: #199249;
  color: white;
  padding: 8px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.add1:hover, a.add1:active {
  background-color: blue;
}


a.update1:link, a.update1:visited {
  background-color: #199249;
  color: white;
  padding: 8px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.update1:hover, a.update1:active {
  background-color: blue;
}


a.uppass:link, a.uppass:visited {
  background-color: #199248;
  color: black;
  padding: 4px 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.uppass:hover, a.uppass:active {
  background-color: blue;
}

a.back:link, a.back:visited {
  background-color: #199248;
  color: black;
  padding: 8px 12px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.back:hover, a.back:active {
  background-color: blue;
}



a.upadmin:link, a.upadmin:visited {
  background-color: #199248;
  color: black;
  padding: 6px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.upadmin:hover, a.upadmin:active {
  background-color: blue;
}

a.dl_dp_admin:link, a.dl_dp_admin:visited {
  background-color: #FF0000;
  color: black;
  padding: 6px 12px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.dl_dp_admin:hover, a.dl_dp_admin:active {
  background-color: blue;
}

a.add_dp_adm:link, a.add_dp_adm:visited {
  background-color: #199248;
  color: white;
  padding: 14px 14px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.add_dp_adm:hover, a.add_dp_adm:active {
  background-color: blue;
}

a.res_pass:link, a.up11pass:visited {
  background-color: #e3ce10;
  color: black;
  padding: 4px 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.res_pass:hover, a.up11pass:active {
  background-color: blue;
}

a.add_a_stud:link, a.add_a_stud:visited {
  background-color: #199248;
  color: black;
  padding: 6px 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.add_a_stud:hover, a.add_a_stud:active {
  background-color: blue;
}


a.backtostulist:link, a.backtostulist:visited {
  background-color: #199248;
  color: black;
  padding: 10px 6px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

a.backtostulist:hover, a.backtostulist:active {
  background-color: blue;
}

</style>
		
		
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
			<img src="../image\download.png" alt="" width="200" height="65" class="navbar-brand ps-3">
            <a class="navbar-brand ps-3" href="index.html">SUB EMS_119</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
           <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                   
                </div>
            </form> 
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        
                       
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="su_admin_logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
       <div id="layoutSidenav">
	   
	   
            <div id="layoutSidenav_nav">
                  
				  <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    
					<div class="sb-sidenav-menu">
                        
					
					<div class="nav">
                            

							<div class="sb-sidenav-menu-heading">Super Admin
							</div>
                               
							   <a class="nav-link" href="su_admin_dashboard.php">
                                   <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                                 </a>
                            <div class="sb-sidenav-menu-heading">Interface
							</div>
                             <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Profile
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                   <a class="nav-link" href="su_admin_profile.php">Change my profile</a>
                                   
                                </nav>
                            </div>
							
							
							<a class="nav-link" href="departments.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                               Manage Departments
                            </a>
							
							
							<a class="nav-link" href="department_admins.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                               Manage Dept. Admins
                            </a>
							
							
							<a class="nav-link" href="stu_list_for_su.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                               Manage Students
                            </a>
							
								<a class="nav-link" href="aca_stf_list_for_su.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                               Manage Academic Staff
                            </a>
							
							
								<a class="nav-link" href="non_aca_stf_list_for_su.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                               Manage Non Academic Staff
                            </a>
							
							
						
						</div>
					
					
					</div>		
							
							<div class="sb-sidenav-footer">
                          <div class="small">Logged in as:</div>
						<!--<?php	echo $user_id; ?> -->
						Super Admin
							</div>  
                           
                        
			      
                   </nav>
			
          
		  </div>
         
		 <div id="layoutSidenav_content">
                <main>
                  

				  <div class="container-fluid px-4">	
				 <!--  <h1 style="color:#3034B0;" class="mt-4">Super Admin </h1> -->
				   